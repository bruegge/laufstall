package de.mpg.mis.bruegge.niffler;

public class Niffler 
{
  public int fassungsvermoegen;
  protected int goldImBeutel;
  String farbe;
  private boolean gierig;
  

  public Niffler() 
  {
    fassungsvermoegen=7;
    farbe="rotbraun";
  }
  
  public Niffler(int fassungsvermoegen, String farbe)
  {
    this.farbe=farbe;
    this.fassungsvermoegen=fassungsvermoegen;
  }

  public String tooString()
  {
    String test=super.toString();
    String endstring="Dieser "+farbe+"e Niffler ist "+gierig+" und kann "+fassungsvermoegen+" Goldstuecke tragen. Im Moment hat er "+goldImBeutel+" Goldstuecke im Beutel.";
    return test+"\n"+endstring; 
  }
  
  public void hierNimm(int angeboteneGoldstuecke)
  {
    goldImBeutel=goldImBeutel+angeboteneGoldstuecke;
  }
  
  public static void main (String[] args)
  {
    Niffler ersterNiffler=new Niffler(10,"gelb");
    Niffler zweiterNiffler=new Niffler();
    System.out.println(ersterNiffler.tooString());
    System.out.println(zweiterNiffler.tooString());
    ersterNiffler.hierNimm(3);
    System.out.println(ersterNiffler.tooString());
  }
  
}



    