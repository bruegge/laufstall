package de.mpg.mis.berger.arrays;

import de.mpg.mis.bruegge.niffler.Niffler;

public class Kettenglied 
{
  protected Object payload;
  protected Kettenglied nextKettenglied;
  protected de.mpg.mis.bruegge.obscurus.Niffler suess;
  protected de.mpg.mis.library.Krabbelgruppe.Niffler suesser;
  
  public Kettenglied(Object inhalt) 
  {
    payload=inhalt;
  }
  
  public Kettenglied add(Object inhalt)
  {
    Kettenglied next=new Kettenglied(inhalt);
    nextKettenglied=next;
    return next;
  }
  
  
  public static void main(String[] args)
  {
    Kettenglied eins=new Kettenglied('B');
    Kettenglied last=eins.add('o');
    last=last.add('w');
    last=last.add('t');
    last=last.add('r');
    last=last.add('u');
    last=last.add('c');
    last=last.add('k');
    last=last.add('l');
    last=last.add('e');
    gibAus(eins);
  }
  
  protected static void gibAus(Kettenglied start)
  {
    while(start!=null)
    {
      System.out.println(start.payload);
      start=start.nextKettenglied;
    }
  }

}
