package de.mpg.mis.berger.arrays;

public class Variablen
{
  protected static String begruesster="Niffler";
  
  public Variablen()
  {
    begruesster="Bowtruckle";
  }
  
  public Variablen(String begruesster)
  {
    this.begruesster=begruesster;
  }

  public static void main(String[] args)
  {
    Variablen test=new Variablen(args[1]);
    Variablen test2=new Variablen(args[0]);
    Variablen test3=new Variablen();
//    System.out.println("Hello World!");
    System.out.println(test.begruesster);
    System.out.println(test2.begruesster);
    System.out.println(test3.begruesster);
  }

}
